function compareArrays(arr1, arr2) {
  if (arr1.length !== arr2.length) {
    return false;
  }

  return arr1.every(function (element, index) {
    return element === arr2[index];
  });
}

function advancedFilter(arr) {
  return arr.filter(function (num) {
    return num >= 0 && num % 3 === 0;
  })
  .map(function (num) {
    return num * 10;
  });
}
