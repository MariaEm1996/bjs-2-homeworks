function parseCount(num) {
    const result = Number.parseInt(num);

    if (isNaN(result)) {
        throw new Error('Невалидное значение');
    }

    return result;
}

function validateCount(num) {
    try {
        return parseCount(num);
    } catch (error) {
        return error;
    }
}

class Triangle {
    constructor(a, b, c) {
        if (((a + b) < c) || ((b + c) < a) || ((c + a) < b)) {
            throw new Error('Треугольник с такими сторонами не существует');
        }

        this.a = a;
        this.b = b;
        this.c = c;
    }

    getPerimeter() {
        return this.a + this.b + this.c;
    }

    getArea() {
        const p = this.getPerimeter() / 2;

        return Number(Math.sqrt(p * (p - this.a) * (p - this.b) * (p - this.c)).toFixed(3));
    }
}

function getTriangle(a, b, c) {
    try {
        return new Triangle(a, b, c);
    } catch (error) {
        return {
            getPerimeter() {
                return 'Ошибка! Треугольник не существует';
            },
            getArea() {
                return 'Ошибка! Треугольник не существует';
            }
        }
    }
}